FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code/build/sope /app/code/build/sogo

RUN apt-get update && \
    apt-get install -y gobjc libgnustep-base-dev gnustep-base-common gnustep-make libldap2-dev libssl-dev libpq-dev libwbxml2-dev libmemcached-dev memcached && \
    rm -rf /var/cache/apt /var/lib/apt/lists

WORKDIR /app/code/build/sope
RUN curl -SLf "https://github.com/inverse-inc/sope/archive/SOPE-4.0.4.tar.gz" | tar -zx --strip-components 1 -f - -C /app/code/build/sope && \
    ./configure --with-gnustep && make && make install && \
    rm -rf /app/code/build/sope

WORKDIR /app/code/build/sogo
RUN curl -SLf "https://github.com/inverse-inc/sogo/archive/SOGo-4.0.4.tar.gz" | tar -zx --strip-components 1 -f - -C /app/code/build/sogo && \
    ./configure && make && make install && \
    rm -rf /app/code/build/sope

RUN mkdir -p /etc/sogo && \
    rm -rf /etc/sogo/sogo.conf && ln -s /run/sogo.conf /etc/sogo/sogo.conf && \
    rm -rf /var/log/nginx && ln -s /run/nginx /var/log/nginx && \
    mkdir /run/GNUstep && ln -s /run/GNUstep /home/cloudron/GNUstep

# configure supervisor
RUN crudini --set /etc/supervisor/supervisord.conf supervisord logfile /run/supervisord.log && \
	crudini --set /etc/supervisor/supervisord.conf supervisord logfile_backups 0
ADD supervisor/ /etc/supervisor/conf.d/

ADD nginx.conf start.sh /app/code/

RUN chown -R cloudron:cloudron /etc/sogo
# Create the spool directory on /run and symlink the original one
RUN rm -rf /var/spool/sogo && mkdir -p /app/data/spool && ln -s /app/data/spool /var/spool/sogo

ENV LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib/sogo

WORKDIR /app/code
CMD [ "/app/code/start.sh" ]
