This application is pre-setup to use Cloudron email. Login with the email and
Cloudron password to access the mailbox.

Email accounts on this Cloudron can be managed [here](/#/email).