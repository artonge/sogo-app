#!/bin/bash

set -eu -o pipefail

echo
echo "##########################"
echo "##     SOGo startup     ##"
echo "##########################"
echo

echo "=> Create sogo.conf"
cat <<EOF > /run/sogo.conf
{
    SOGoProfileURL = "${MYSQL_URL}/sogo_user_profile";
    OCSFolderInfoURL = "${MYSQL_URL}/sogo_folder_info";
    OCSSessionsFolderURL = "${MYSQL_URL}/sogo_sessions_folder";
    OCSEMailAlarmsFolderURL = "${MYSQL_URL}/sogo_alarms_folder";
    SOGoLanguage = English;
    SOGoAppointmentSendEMailNotifications = YES;
    SOGoFoldersSendEMailNotifications = NO;
    SOGoACLsSendEMailNotifications = NO;
    SOGoNotifyOnPersonalModifications = NO;
    SOGoNotifyOnExternalModifications = NO;
    SOGoTimeZone = UTC;
    SOGoVacationEnabled = NO;
    SOGoForwardEnabled = NO;
    SOGoFirstDayOfWeek = 0;
    WOPort = "0.0.0.0:4000";
    WOLogFile = "/run/sogo.log";
    WONoDetach = YES;
    WOPidFile = "/run/sogo.pid";
    WOWorkersCount = 5;
    WOUseRelativeURLs = YES;
    SOGoMailAuxiliaryUserAccountsEnabled = NO;
    SOGoLoginModule = "Mail";
    SOGoMemcachedHost = "127.0.0.1:11211";
    SOGoGravatarEnabled = YES;
    SOGoMailCustomFromEnabled = YES;
    SOGoEnableDomainBasedUID = YES;
    domains = {
EOF

mail_configs=$(echo ${MAIL_DOMAINS} | tr "," "\n")

# old versions (pre 3.4.3) of cloudron did not inject this env var
LDAP_MAILBOXES_BASE_DN="${LDAP_MAILBOXES_BASE_DN:-ou=mailboxes,dc=cloudron}"

for domain in $mail_configs
do
cat <<EOF >> /run/sogo.conf
        ${domain} = {
            SOGoMailDomain = ${domain};
            SOGoIMAPServer = "imaps://${MAIL_IMAP_SERVER}:${MAIL_IMAP_PORT}";
            SOGoSMTPServer = ${MAIL_SMTP_SERVER}:${MAIL_SMTP_PORT};
            SOGoSieveServer = "sieve://${MAIL_SIEVE_SERVER}:${MAIL_SIEVE_PORT}/?tls=YES";
            SOGoMailingMechanism = smtp;
            SOGoSMTPAuthenticationType = PLAIN;
            SOGoSieveScriptsEnabled = YES;
            SOGoSieveFolderEncoding = UTF-8;
            SOGoUserSources = ({
                type = ldap;
                CNFieldName = cn;
                IDFieldName = cn;
                UIDFieldName = mail;
                baseDN = "domain=${domain},${LDAP_MAILBOXES_BASE_DN}";
                bindDN = "${LDAP_BIND_DN}";
                bindPassword = "${LDAP_BIND_PASSWORD}";
                canAuthenticate = YES;
                hostname = ${LDAP_URL};
                id = cloudron_ldap_${domain};
                isAddressBook = NO;
                MailFieldNames = ( "alias" );
            });
        };
EOF
done

cat <<EOF >> /run/sogo.conf
    };
}
EOF

echo "=> Generating nginx.conf"
sed -e "s,##HOSTNAME##,${APP_DOMAIN}," \
    /app/code/nginx.conf  > /run/nginx.conf

echo "=> Ensure directories"
mkdir -p /run/GNUstep /run/nginx

echo "=> Make cloudron own the data"
mkdir -p /app/data/spool
chown -R cloudron:cloudron /run /app/data

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i SOGo
