#!/usr/bin/env node

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    until = webdriver.until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (!process.env.EMAIL || !process.env.PASSWORD) {
    console.log('EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test';
    var EVENT_TITLE = 'Meet the Cloudron Founders';
    var CONTACT_CN = 'Max Mustermann';
    var TEST_TIMEOUT = 200000;
    var app;

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    }

    function login(done) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn);
        }).then(function () {
            return waitForElement(by.id('input_1'));
        }).then(function () {
            return browser.findElement(by.id('input_1')).sendKeys(process.env.EMAIL);
        }).then(function () {
            return browser.findElement(by.id('input_2')).sendKeys(process.env.PASSWORD);
        }).then(function () {
            return browser.findElement(by.name('loginForm')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[text()="Inbox"]')), TEST_TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function addContact(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.EMAIL + '/Contacts/view#/addressbooks/personal/card/new').then(function () {
            return waitForElement(by.xpath('//*[@aria-label="New Contact"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[@aria-label="New Contact"]')).click();
        }).then(function () {
            // open animation
            return browser.sleep(2000);
        }).then(function () {
            return waitForElement(by.xpath('//*[@aria-label="Create a new address book card"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[@aria-label="Create a new address book card"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//*[@ng-model="editor.card.c_cn"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[@ng-model="editor.card.c_cn"]')).sendKeys(CONTACT_CN);
        }).then(function () {
            return browser.findElement(by.xpath('//*[@aria-label="Save"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//div[text()="' + CONTACT_CN + '"]')), TEST_TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function getContact(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.EMAIL + '/Contacts/view').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//div[text()="' + CONTACT_CN + '"]')), TEST_TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function eventExists(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.EMAIL + '/Calendar/view').then(function () {
            return browser.sleep(2000);
        }).then(function () {
            return waitForElement(by.xpath('//*[text()[contains(., "' + EVENT_TITLE + '")]]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[text()[contains(., "' + EVENT_TITLE + '")]]')).click();
        }).then(function () {
            // wait for open
            return browser.sleep(2000);
        }).then(function () {
            return waitForElement(by.xpath('//*[text()="' + EVENT_TITLE + '"]'));
        }).then(function () {
            done();
        });
    }

    function visitMailbox(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.EMAIL + '/Mail/view').then(function () {
            return waitForElement(by.xpath('//*[@aria-label="Write a new message"]'));
        }).then(function () {
            done();
        });
    }

    function visitFilters(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.EMAIL + '/Preferences#!/mailer').then(function () {
            return browser.sleep(3000);
        }).then(function () {
            return browser.findElement(by.xpath('//*[text()[contains(., "Filters")]]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//*[@aria-label="Create Filter"]'));
        }).then(function () {
            done();
        });
    }

    function createEvent(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.EMAIL + '/Calendar/view').then(function () {
            return waitForElement(by.xpath('//*[@aria-label="New Event"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[@aria-label="New Event"]')).click();
        }).then(function () {
            // open animation
            return browser.sleep(2000);
        }).then(function () {
            return waitForElement(by.xpath('//*[@aria-label="Create a new event"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[@aria-label="Create a new event"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//*[@ng-model="editor.component.summary"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[@ng-model="editor.component.summary"]')).sendKeys(EVENT_TITLE);
        }).then(function () {
            return browser.findElement(by.xpath('//*[@ng-model="editor.component.summary"]')).submit();
        }).then(function () {
            // wait for save
            return browser.sleep(2000);
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login);

    it('can create event', createEvent);

    it('event is present', eventExists);
    it('can add contact', addContact);
    it('can get contact', getContact);
    it('can visit mailbox', visitMailbox);
    it('can visit filters', visitFilters);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('event is still present', eventExists);
    it('can get contact', getContact);
    it('can visit mailbox', visitMailbox);
    it('can visit filters', visitFilters);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('event is still present', eventExists);
    it('can get contact', getContact);
    it('can visit mailbox', visitMailbox);
    it('can visit filters', visitFilters);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // update tests
    it('can install app for update', function () {
        execSync('cloudron install --new --wait --appstore-id nu.sogo.cloudronapp2 --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add contact', addContact);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can get contact', getContact);
    it('can visit filters', visitFilters);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
